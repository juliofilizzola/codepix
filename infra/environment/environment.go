package environment

import (
	"fmt"
	"log"
	"os"

	"github.com/joho/godotenv"
)

var (
	KafkaBootstrap                    = ""
	KafkaConsumerGroup                = ""
	KafkaTransactionTopic             = ""
	KafkaTransactionConfirmationTopic = ""
	Port                              = ""
	DBType                            = ""
	DNS                               = ""
	ENV                               = ""
	AutoMigrate                       = false
	DEBUG                             = false
)

func Environment() {
	var err error

	if err = godotenv.Load(); err != nil {
		log.Fatal(err)
	}
	ENV = fmt.Sprintf(os.Getenv("ENV"))
	KafkaBootstrap = fmt.Sprintf(os.Getenv("KAFKA_BOOTSTRAP_SERVER"))
	KafkaConsumerGroup = fmt.Sprintf(os.Getenv("KAFKA_CONSUMER_GROUP"))
	KafkaTransactionTopic = fmt.Sprintf(os.Getenv("KAFKA_TRANSACTION_TOPIC"))
	KafkaTransactionConfirmationTopic = fmt.Sprintf(os.Getenv("KAFKA_TRANSACTION_CONFIRMATION_TOPIC"))

	Port = fmt.Sprint(os.Getenv("PORT"))
	if ENV != "test" {
		DNS = fmt.Sprint(os.Getenv("DNS"))
		DBType = fmt.Sprint(os.Getenv("DB_TYPE"))
	} else {
		DNS = fmt.Sprint(os.Getenv("DNS_TEST"))
		DBType = fmt.Sprint(os.Getenv("DB_TYPE_TEST"))
	}

	migrate := fmt.Sprint(os.Getenv("AUTO_MIGRATE"))

	if migrate == "true" {
		AutoMigrate = true
	}

	debugDb := fmt.Sprint(os.Getenv("DEBUG"))

	if debugDb == "true" {
		DEBUG = true
	}
}
