package db

import (
	"log"

	"CodePix/domain/model"
	"CodePix/infra/environment"
	"github.com/jinzhu/gorm"
	_ "github.com/lib/pq"
	_ "gorm.io/driver/sqlite"
)

func init() {
	environment.Environment()
}

func Connect() *gorm.DB {
	var db *gorm.DB
	var err error
	db, err = gorm.Open(environment.DBType, environment.DNS)

	if err != nil {
		log.Fatalf("Error connecting to database: %v", err)
		panic(err)
	}

	db.LogMode(environment.DEBUG)

	if environment.AutoMigrate {
		db.AutoMigrate(&model.Bank{}, &model.Account{}, &model.PixKey{}, &model.Transaction{})
	}
	return db
}
