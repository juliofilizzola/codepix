package repository

import (
	"fmt"

	"CodePix/domain/model"
	"github.com/jinzhu/gorm"
)

type AccountRepository struct {
	Db *gorm.DB
}

func (r AccountRepository) FindBank(id string) (*model.Bank, error) {
	var bank model.Bank
	r.Db.First(&bank, "id = ?", id)

	if bank.ID == "" {
		return nil, fmt.Errorf("no bank found")
	}
	return &bank, nil
}

func (r AccountRepository) CreateAccount(account *model.Account) (*model.Account, error) {
	err := r.Db.Create(account).Error
	if err != nil {
		return nil, err
	}
	return account, nil
}

func (r AccountRepository) FindAccountByEmail(email string) (*model.Account, error) {
	var account model.Account
	var bank model.Bank
	r.Db.Preload("Account").First(&account, "email = ?", email)
	if account.ID == "" {
		return nil, fmt.Errorf("no account was found")
	}
	r.Db.Preload("Bank").Find(&bank, "id = ?", account.BankId)

	account.Bank = &bank
	return &account, nil
}
