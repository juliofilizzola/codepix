package repository

import (
	"errors"

	"CodePix/domain/model"
	"github.com/jinzhu/gorm"
)

type BankRepository struct {
	Db *gorm.DB
}

func (b BankRepository) FindAll() []*model.Bank {
	var banks []*model.Bank
	b.Db.Table("banks").Find(&banks)
	return banks
}

func (b BankRepository) FindByCode(code string) (*model.Bank, error) {
	var bank model.Bank

	b.Db.Preload("Bank").First(&bank, "code = ?", code)

	if bank.ID == "" {
		return nil, errors.New("bank not found")
	}

	return &bank, nil
}
