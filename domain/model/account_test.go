package model_test

import (
	"testing"

	"CodePix/domain/model"
	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/require"
)

func TestNewAccount(t *testing.T) {
	code := "003"
	name := "New Bank"
	bank, err := model.NewBank(code, name)

	accountNumber := "23123232323"
	ownerName := "Fulano"
	account, err := model.NewAccount(bank, accountNumber, ownerName)

	require.Nil(t, err)

	require.NotEmpty(t, uuid.FromStringOrNil(account.ID))
	require.Equal(t, account.Number, accountNumber)
	// require.Equal(t, account.BankID, bank.ID)
	//
	// _, err = model.NewAccount(bank, "", ownerName)
	// require.NotNil(t, err)
}
