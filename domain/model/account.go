package model

import (
	"time"

	"github.com/asaskevich/govalidator"
	uuid "github.com/satori/go.uuid"
)

type AccountRepositoryInterface interface {
	CreateAccount(account *Account) (*Account, error)
	FindAccountByEmail(email string) (*Account, error)
	FindBank(bankId string) (*Bank, error)
}

type Account struct {
	Base      `valid:"required"`
	Email     string    `json:"email" gorm:"type:varchar(255);not null;unique" valid:"notnull"`
	OwnerName string    `gorm:"column:owner_name;type:varchar(255);not null" json:"owner_name" valid:"notnull"`
	Bank      *Bank     `valid:"-"`
	BankId    string    `gorm:"column:bank_id;type:uuid;not null" json:"bank_id" valid:"-"`
	Number    string    `json:"number" valid:"notnull"`
	PixKeys   []*PixKey `gorm:"ForeignKey:AccountID" valid:"-"`
}

func (account *Account) isValid() error {
	_, err := govalidator.ValidateStruct(account)
	if err != nil {
		return err
	}
	return nil
}

func NewAccount(bank *Bank, number string, ownerName string, email string) (*Account, error) {
	account := Account{
		OwnerName: ownerName,
		Bank:      bank,
		Number:    number,
		Email:     email,
	}
	account.ID = uuid.NewV4().String()
	account.CreatedAt = time.Now()

	err := account.isValid()
	if err != nil {
		return nil, err
	}

	return &account, nil
}
