/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package main

import (
	"fmt"

	"CodePix/cmd"
	"CodePix/script/seed"
)

func main() {
	err := seed.PopulateBanks()
	if err != nil {
		fmt.Println(err)
	}
	cmd.Execute()
}
