package usecase

import (
	"CodePix/domain/model"
)

type BankUseCase struct {
	BankRepository model.BankRepositoryInterface
}

func (b BankUseCase) FindAll() []*model.Bank {
	banks := b.BankRepository.FindAll()
	return banks
}

func (b BankUseCase) FindByCode(code string) (*model.Bank, error) {
	bank, err := b.BankRepository.FindByCode(code)

	if err != nil {
		return nil, err
	}

	return bank, nil
}
