package usecase

import (
	"errors"
	"math/rand"
	"strconv"

	"CodePix/domain/model"
)

type AccountUseCase struct {
	AccountRepository model.AccountRepositoryInterface
}

func (c *AccountUseCase) CreateAccount(name string, email string, bankId string) (*model.Account, error) {
	accountAlreadyExist, err := c.AccountRepository.FindAccountByEmail(email)

	if accountAlreadyExist != nil || err == nil {
		return nil, errors.New("account already exist")
	}

	bank, err := c.AccountRepository.FindBank(bankId)

	if err != nil {
		return nil, errors.New("bank not found")
	}

	accountNumber := rand.Intn(100)

	account, err := model.NewAccount(bank, strconv.Itoa(accountNumber), name, email)

	if err != nil {
		return nil, err
	}

	createAccount, err := c.AccountRepository.CreateAccount(account)

	if err != nil {
		return nil, err
	}

	return createAccount, nil
}

func (c *AccountUseCase) FindAccount(email string) (*model.Account, error) {
	account, err := c.AccountRepository.FindAccountByEmail(email)

	if err != nil {
		return nil, err
	}

	return account, nil
}
