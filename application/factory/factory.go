package factory

import (
	"CodePix/application/usecase"
	"CodePix/infra/db"
	"CodePix/infra/repository"
)

func TransactionUseCaseFactory() usecase.TransactionUseCase {
	database := db.Connect()
	pixRepository := repository.PixKeyRepositoryDb{Db: database}
	transactionRepository := repository.TransactionRepositoryDb{Db: database}

	transactionUseCase := usecase.TransactionUseCase{
		TransactionRepository: &transactionRepository,
		PixRepository:         pixRepository,
	}

	return transactionUseCase
}
