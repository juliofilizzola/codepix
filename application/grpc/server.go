package grpc_server

import (
	"fmt"
	"log"
	"net"

	"CodePix/application/grpc/pb"
	"CodePix/application/usecase"
	"CodePix/infra/environment"
	"CodePix/infra/repository"
	"github.com/jinzhu/gorm"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func StartGRpcServer(database *gorm.DB) {
	port := environment.Port
	grpcServer := grpc.NewServer()
	reflection.Register(grpcServer)

	pixRepository := repository.PixKeyRepositoryDb{Db: database}
	pixUseCase := usecase.PixUseCase{PixKeyRepository: pixRepository}
	pixGrpcService := NewPixGrpcService(pixUseCase)
	pb.RegisterPixServiceServer(grpcServer, pixGrpcService)

	accountRepository := repository.AccountRepository{
		Db: database,
	}
	accountUseCase := usecase.AccountUseCase{AccountRepository: accountRepository}
	accountGrpcService := NewBankAccountClient(accountUseCase)
	pb.RegisterAccountServiceServer(grpcServer, accountGrpcService)

	bankRepository := repository.BankRepository{
		Db: database,
	}

	bankUseCase := usecase.BankUseCase{
		BankRepository: bankRepository,
	}

	bankGrpcService := NewBankClient(bankUseCase)
	pb.RegisterBankServiceServer(grpcServer, bankGrpcService)

	address := fmt.Sprintf("0.0.0.0:%s", port)
	listener, err := net.Listen("tcp", address)
	if err != nil {
		log.Fatal("cannot start grpc server", err)
	}

	log.Printf("gRPC server has been started on port %s", port)
	err = grpcServer.Serve(listener)
	if err != nil {
		log.Fatal("cannot start grpc server", err)
	}
}
