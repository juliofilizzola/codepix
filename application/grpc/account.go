package grpc_server

import (
	"context"

	"CodePix/application/grpc/pb"
	"CodePix/application/usecase"
)

type AccountGrpcService struct {
	AccountUseCase usecase.AccountUseCase
	pb.UnimplementedAccountServiceServer
}

func (a *AccountGrpcService) CreateAccount(ctx context.Context, in *pb.BankAccountRegistration) (*pb.CreateAccountReturn, error) {
	account, err := a.AccountUseCase.CreateAccount(in.Name, in.Email, in.BankId)

	if err != nil {
		return &pb.CreateAccountReturn{
			Status: "not created",
			Error:  err.Error(),
		}, err
	}

	return &pb.CreateAccountReturn{
		Id:     account.ID,
		Status: "created",
	}, nil
}

func (a *AccountGrpcService) FindAccount(ctx context.Context, in *pb.BodyFindAccount) (*pb.AccountInfo, error) {
	account, err := a.AccountUseCase.FindAccount(in.Email)

	if err != nil {
		return &pb.AccountInfo{}, err
	}

	return &pb.AccountInfo{
		AccountId:     account.ID,
		AccountNumber: account.Number,
		BankId:        account.BankId,
		BankName:      account.Bank.Name,
		OwnerName:     account.OwnerName,
		CreatedAt:     account.CreatedAt.String(),
	}, nil

}

func NewBankAccountClient(useCase usecase.AccountUseCase) *AccountGrpcService {
	return &AccountGrpcService{
		AccountUseCase: useCase,
	}
}
