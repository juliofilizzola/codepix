package grpc_server

import (
	"context"
	"errors"

	"CodePix/application/grpc/pb"
	"CodePix/application/usecase"
)

type BankGrpcService struct {
	BankUseCase usecase.BankUseCase
	pb.UnimplementedBankServiceServer
}

func (b BankGrpcService) FindAllBank(ctx context.Context, in *pb.Empty) (*pb.BankInfo, error) {
	var banks pb.BankInfo

	bs := b.BankUseCase.FindAll()

	if len(bs) < 1 {
		return nil, errors.New("bank not found")
	}

	bank := make([]*pb.Bank, 0, len(bs))

	for _, v := range bs {
		bank = append(bank, &pb.Bank{
			BankId:   v.ID,
			BankCode: v.Code,
			BankName: v.Name,
		})
	}

	banks.Bank = bank

	return &banks, nil
}

func (b BankGrpcService) FindByCode(ctx context.Context, in *pb.ByCode) (*pb.Bank, error) {
	bankN, err := b.BankUseCase.FindByCode(in.Code)
	if err != nil {
		return &pb.Bank{}, err
	}
	return &pb.Bank{
		BankId:   bankN.ID,
		BankCode: bankN.Code,
		BankName: bankN.Name,
	}, nil
}

func NewBankClient(useCase usecase.BankUseCase) *BankGrpcService {
	return &BankGrpcService{
		BankUseCase: useCase,
	}
}
