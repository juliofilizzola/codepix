package kafka

import (
	"fmt"
	"log"

	"CodePix/application/factory"
	appModel "CodePix/application/model"
	"CodePix/application/usecase"
	"CodePix/infra/environment"
	"github.com/jinzhu/gorm"

	ka "github.com/confluentinc/confluent-kafka-go/kafka"
)

func init() {
	environment.Environment()
}

type ProcessKafka struct {
	Database        *gorm.DB
	Producer        *ka.Producer
	DeliveryChannel chan ka.Event
}

func NewKafkaProcess(db *gorm.DB, producer *ka.Producer, deliveryChannel chan ka.Event) *ProcessKafka {
	return &ProcessKafka{
		Database:        db,
		DeliveryChannel: deliveryChannel,
		Producer:        producer,
	}
}

func (k *ProcessKafka) Consumer() {
	configMap := &ka.ConfigMap{
		"bootstrap.servers": environment.KafkaBootstrap,
		"group.id":          environment.KafkaConsumerGroup,
		"auto.offset.reset": "earliest",
	}
	c, err := ka.NewConsumer(configMap)

	if err != nil {
		log.Fatal("err: ", err)
	}

	topics := []string{environment.KafkaTransactionTopic, environment.KafkaTransactionConfirmationTopic}
	err = c.SubscribeTopics(topics, nil)

	if err != nil {
		log.Fatal("err: ", err)
	}

	for {
		msg, err := c.ReadMessage(-1)
		fmt.Println("msg: ", msg)
		if err == nil {
			if err == nil {
				fmt.Println(string(msg.Value))
				k.processMessage(msg)
			}
		}
	}
}

func (k *ProcessKafka) processMessage(msg *ka.Message) {
	transactionTopic := "transaction"
	transactionConfirmedTopic := "transaction-confirmation"

	switch topic := *msg.TopicPartition.Topic; topic {
	case transactionTopic:
		err := k.processTransaction(msg)
		if err != nil {
			log.Fatal(err)
		}
	case transactionConfirmedTopic:
		err := k.processTransactionConfirmation(msg)
		if err != nil {
			log.Fatal(err)
		}
	default:
		fmt.Println("not valid topic", string(msg.Value))
	}
}

func (k *ProcessKafka) processTransaction(msg *ka.Message) error {
	transaction := appModel.NewTransaction()
	err := transaction.ParseJson(msg.Value)
	if err != nil {
		return err
	}

	transactionUseCase := factory.TransactionUseCaseFactory()
	createdTransaction, err := transactionUseCase.Register(
		transaction.AccountID,
		transaction.Amount,
		transaction.PixKeyTo,
		transaction.PixKeyKindTo,
		transaction.Description,
	)

	if err != nil {
		return err
	}

	topic := "bank" + createdTransaction.PixKeyTo.Account.Bank.Code

	transaction.ID = createdTransaction.ID

	transaction.Status = "pending"

	transactionJson, err := transaction.ToJson()

	if err != nil {
		return err
	}

	err = Publish(string(transactionJson), topic, k.Producer, k.DeliveryChannel)
	if err != nil {
		return err
	}

	return nil
}

func (k *ProcessKafka) processTransactionConfirmation(msg *ka.Message) error {
	transaction := appModel.NewTransaction()
	err := transaction.ParseJson(msg.Value)
	if err != nil {
		return err
	}

	transactionUseCase := factory.TransactionUseCaseFactory()

	if transaction.Status == appModel.TransactionConfirmed {
		err = k.confirmTransaction(transaction, transactionUseCase)
		if err != nil {
			return err
		}
	} else if transaction.Status == appModel.TransactionConfirmed {
		_, err := transactionUseCase.Complete(transaction.ID)
		if err != nil {
			return err
		}
	}

	return nil
}

func (k *ProcessKafka) confirmTransaction(transaction *appModel.Transaction, transactionUseCase usecase.TransactionUseCase) error {
	confirmedTransaction, err := transactionUseCase.Confirm(transaction.ID)
	if err != nil {
		return err
	}

	topic := "bank" + confirmedTransaction.AccountFrom.Bank.Code
	transactionJson, err := transaction.ToJson()
	if err != nil {
		return err
	}

	err = Publish(string(transactionJson), topic, k.Producer, k.DeliveryChannel)
	if err != nil {
		return err
	}
	return nil
}
