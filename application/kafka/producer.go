package kafka

import (
	"fmt"
	"log"

	"CodePix/infra/environment"
	ka "github.com/confluentinc/confluent-kafka-go/kafka"
)

func init() {
	environment.Environment()
}

func NewKafkaProducer() *ka.Producer {
	configMap := &ka.ConfigMap{
		"bootstrap.servers": environment.KafkaBootstrap,
	}

	p, err := ka.NewProducer(configMap)
	if err != nil {
		log.Fatal(err)
	}

	return p
}

func Publish(msg string, topic string, producer *ka.Producer, deliveryChannel chan ka.Event) error {
	message := &ka.Message{
		TopicPartition: ka.TopicPartition{Topic: &topic, Partition: ka.PartitionAny},
		Value:          []byte(msg),
	}
	err := producer.Produce(message, deliveryChannel)

	if err != nil {
		return err
	}
	return nil
}

func DeliveryReport(deliveryChan chan ka.Event) {
	for e := range deliveryChan {
		switch ev := e.(type) {
		case *ka.Message:
			if ev.TopicPartition.Error != nil {
				fmt.Println("Delivery failed:", ev.TopicPartition)
			} else {
				fmt.Println("Delivered message to:", ev.TopicPartition)
			}
		}
	}
}
