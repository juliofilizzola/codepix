package seed

import (
	"encoding/json"
	"errors"
	"os"

	"CodePix/domain/model"
	"CodePix/infra/db"
)

func PopulateBanks() error {
	data, err := os.ReadFile("data/banks.json")
	if err != nil {
		return err
	}

	var banks []*model.Bank
	var allBanks []*model.Bank

	if err := json.Unmarshal(data, &banks); err != nil {
		return err
	}

	if len(banks) == 0 {
		return errors.New("no banks parsed")
	}
	database := db.Connect()
	// validate banks
	database.Find(&allBanks)

	if len(allBanks) > 1 {
		return nil
	}
	for _, dataBank := range banks {
		bank, err := model.NewBank(dataBank.Code, dataBank.Name)
		if err != nil {
			continue
		}
		if err := database.Save(bank).Error; err != nil {
			return err
		}
	}

	return nil
}
