/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"fmt"

	"CodePix/application/kafka"
	"CodePix/infra/db"
	ka "github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/spf13/cobra"
)

// kafkaCmd represents the kafka command
var kafkaCmd = &cobra.Command{
	Use:   "kafka",
	Short: "A brief description of your command",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Kafka producer message")

		database := db.Connect()
		producer := kafka.NewKafkaProducer()
		deliveryChannel := make(chan ka.Event)
		kafka.Publish("Hello new Topic Kafka", "t", producer, deliveryChannel)
		go kafka.DeliveryReport(deliveryChannel)

		kafkaProcess := kafka.NewKafkaProcess(database, producer, deliveryChannel)

		kafkaProcess.Consumer()
	},
}

func init() {
	rootCmd.AddCommand(kafkaCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// kafkaCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// kafkaCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
